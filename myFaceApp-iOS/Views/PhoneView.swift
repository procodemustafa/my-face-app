//
//  phoneView.swift
//  myFaceApp-iOS
//
//  Created by Mustafa Alkhazraji on 29.11.2020.
//

import SwiftUI
import iPhoneNumberField
import FirebaseCore
import FirebaseAuth

struct PhoneView: View {
    @Binding var showPhone: Bool
    
    @ObservedObject var passcodeModel = PasscodeModel(passCodeLength: 6)
    @State var showLogin = false
    @State private var attempts: Int = 0
    @State var showVerification = false
    @State var phoneText = ""
    @State var formatted = false

    var body: some View {
            VStack {
                Text("Sign up")
                    .font(.title)
                    .fontWeight(.medium)
                    .foregroundColor(Color.init(UIColor(red: 0.63, green: 0.70, blue: 0.77, alpha: 1.00)))
                    .multilineTextAlignment(.center)
                    .padding(.bottom, 10)
                
                Text(showVerification ? "Enter Verification Code" : "Enter your phone number")
                    .fontWeight(.bold)
                    .foregroundColor(Color.init(UIColor(red: 0.63, green: 0.70, blue: 0.77, alpha: 1.00)))
                    .multilineTextAlignment(.leading)
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .padding(.bottom, 30)
                if showVerification {
                    HStack {
                        PassCodeInputField(inputModel: self.passcodeModel)
                            .modifier(Shake(animatableData: CGFloat(attempts)))
                    }
                    .padding(.bottom, 25)
                    .frame(minWidth: 0, maxWidth: .infinity)
                } else {
                    HStack() {
                        iPhoneNumberField(text: $phoneText)
                            .foregroundColor(UIColor.white)
                            .flagHidden(false)
                            .flagSelectable(false)
                            .prefixHidden(false)
                            .foregroundColor(Color.white)
                            .padding(.leading, 20)
                            .modifier(Shake(animatableData: CGFloat(attempts)))
                        
                    }
                    .frame(height: 50)
                    .background(Color.init(UIColor(red: 0.35, green: 0.41, blue: 0.46, alpha: 1.00)))
                    .font(.title)
                    .multilineTextAlignment(.center)
                    .cornerRadius(10)
                    .shadow(radius: 10, x: 3, y: 6)
                    .padding(.leading, 25)
                    .padding(.trailing, 25)
                    .padding(.bottom, 25)
                    .keyboardType(.numberPad)
                }
                
                Button(action: {
                    if showVerification {
                        if self.phoneText == "" {
                            withAnimation(.default) {
                                self.attempts += 1
                            }
                        }
                    }
                    print("Phone Number is \(self.phoneText)")
                    let number = "+" + phoneText.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
                    print("unformatted number \(number)")
                   // let locale = Locale.current
                    //print(Locale.current.regionCode)
                    self.showVerification = true
                }) {
                    Text(showVerification ? "Confirm" : "Send Code")
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .frame(height: 50)
                        .background(Color.init(UIColor(red: 0.53, green: 0.94, blue: 0.60, alpha: 1.00)))
                        .foregroundColor(.white)
                        .cornerRadius(10)
                        .padding(.leading, 25)
                        .padding(.trailing, 25)

                }
                .padding(.bottom, 10)
                //.disabled(self.passcodeModel.isValid)

                
                Button(action: {
                    if showVerification {
                        
                    } else {
                        self.showLogin = true
                    }
                }) {
                    Text(showVerification ? "Resend Verification Code" : "SIGN IN")
                        .font(.subheadline)
                        .foregroundColor(Color.init(UIColor(red: 0.53, green: 0.94, blue: 0.60, alpha: 1.00)))
                        .cornerRadius(10)
                }
                
                Spacer()
                
            }
            .padding(.top, 30)
            .padding(.leading, 10)
            .padding(.trailing, 10)
            .padding(.bottom, 30)
            .animation(.spring(response: 0.8, dampingFraction: 0.9, blendDuration: 0))
        if showLogin {
            Color.init(UIColor(red: 0.29, green: 0.35, blue: 0.40, alpha: 1.00))
                 .edgesIgnoringSafeArea(.all)
            LoginView(showLogin: $showLogin)
        }

    }
}

#if DEBUG
struct PhoneView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.init(UIColor(red: 0.29, green: 0.35, blue: 0.40, alpha: 1.00))
                 .edgesIgnoringSafeArea(.all)
            PhoneView(showPhone: .constant(true))
        }
    }
}
#endif
