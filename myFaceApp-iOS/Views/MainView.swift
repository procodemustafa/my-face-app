//
//  ContentView.swift
//  myFaceApp-iOS
//
//  Created by Mustafa Alkhazraji on 20.11.2020.
//

import SwiftUI


struct MainView: View {

    @State var showLogin = false
    var body: some View {
        ZStack {
            Color.init(UIColor(red: 0.29, green: 0.35, blue: 0.40, alpha: 1.00))
                .edgesIgnoringSafeArea(.all)
            WelcomeView()
        }
        

    }
}



#if DEBUG
struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        Color.init(UIColor(red: 0.29, green: 0.35, blue: 0.40, alpha: 1.00))
             .edgesIgnoringSafeArea(.all)
             .overlay(MainView())
    }
}
#endif

