//
//  OptionsView.swift
//  myFaceApp-iOS
//
//  Created by Mustafa Alkhazraji on 6.12.2020.
//

import SwiftUI

struct OptionView: View {
    
    @State var notificationToggle = false
    @State var nightModeToogle = false
    @State var settingTitle: String = "Settings"
    @State var positionX: CGFloat = 0
    @State var mainView = 0

    @State var currentPassword = ""
    @State var newPassword = ""
    @State var confirmPassword = ""
    
    var body: some View {
        VStack {
            ZStack {
                HStack {
                    Button(action: {
                        positionX = 0
                        settingTitle = "Settings"
                        mainView = 0
                    })
                    {
                        Image(systemName: "arrow.backward")
                            .resizable()
                            .foregroundColor(Color.init(UIColor(red: 0.63, green: 0.70, blue: 0.77, alpha: 1.00)))
                            .frame(width: 24, height: 24, alignment: .center)
                    }
                    .padding(.leading, 10)
                    Spacer()
                }
                HStack {
                    Text(settingTitle)
                        .font(.title)
                        .fontWeight(.medium)
                        .foregroundColor(Color.init(UIColor(red: 0.63, green: 0.70, blue: 0.77, alpha: 1.00)))
                        .multilineTextAlignment(.center)
                }
            }
            .padding()
        
            ZStack {
                ScrollView {
                    SettingsBar(toggleOn: $nightModeToogle, isToggle: false, settingLabel: "Edit Profile", imageName: "Edit", isLabel: false, positionX: $positionX, settingTitle: $settingTitle, mainView: $mainView)
                    SettingsBar(toggleOn: $nightModeToogle, isToggle: false, settingLabel: "Change Password", imageName: "Password", isLabel: false, positionX: $positionX, settingTitle: $settingTitle, mainView: $mainView)
                    SettingsBar(toggleOn: $nightModeToogle, isToggle: false, settingLabel: "Language", imageName: "Language", isLabel: true, positionX: $positionX, settingTitle: $settingTitle, mainView: $mainView)
                    SettingsBar(toggleOn: $notificationToggle, isToggle: true, settingLabel: "Notification", imageName: "Notifications", isLabel: false, positionX: $positionX, settingTitle: $settingTitle, mainView: $mainView)
                    SettingsBar(toggleOn: $nightModeToogle, isToggle: true, settingLabel: "Night Mode", imageName: "Edit", isLabel: false, positionX: $positionX, settingTitle: $settingTitle, mainView: $mainView)
                    SettingsBar(toggleOn: $nightModeToogle, isToggle: false, settingLabel: "Share", imageName: "Share", isLabel: false, positionX: $positionX, settingTitle: $settingTitle, mainView: $mainView)
                    SettingsBar(toggleOn: $nightModeToogle, isToggle: false, settingLabel: "Contact us", imageName: "Send Icon", isLabel: false, positionX: $positionX, settingTitle: $settingTitle, mainView: $mainView)
                    SettingsBar(toggleOn: $nightModeToogle, isToggle: false, settingLabel: "About us", imageName: "Send Icon", isLabel: false, positionX: $positionX, settingTitle: $settingTitle, mainView: $mainView)
                    SettingsBar(toggleOn: $nightModeToogle, isToggle: false, settingLabel: "Sign out", imageName: "Sign out", isLabel: false, positionX: $positionX, settingTitle: $settingTitle, mainView: $mainView)
                }
                .padding(.leading, 20)
                .padding(.trailing, 20)
                .offset(x: (mainView == 0) ? 0 : -500)
                if mainView == 1 {
                    EditProfileView(fullName: "", email: "", age: "", phoneNumber: "")
                } else if mainView == 2 {
                    ChangePasswordView()
                }
            }
           
        }

        .animation(.spring(response: 0.8, dampingFraction: 0.9, blendDuration: 0))
    }
}

struct OptionView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.init(UIColor(red: 0.29, green: 0.35, blue: 0.40, alpha: 1.00))
                 .edgesIgnoringSafeArea(.all)
            OptionView()
        }
    }
}

struct SettingsBar: View {
    @Binding var toggleOn: Bool
    var isToggle: Bool
    var settingLabel: String
    var imageName: String
    var isLabel: Bool
    @Binding var positionX: CGFloat
    @Binding var settingTitle: String
    @Binding var mainView: Int

    
    var body: some View {
        
        if isToggle {
            Toggle(isOn: $toggleOn) {
                Image(imageName)
                    .resizable()
                    .foregroundColor(.white)
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 18, height: 18, alignment: .center)
                    .padding(.trailing, 15)
                Text(settingLabel)
                    .font(.headline)
                    .fontWeight(.medium)
                    .foregroundColor(Color.init(UIColor(red: 0.63, green: 0.70, blue: 0.77, alpha: 1.00)))
                    .multilineTextAlignment(.leading)
                    .lineLimit(1)
            }
            .padding(.leading, 20)
            .padding(.trailing, 20)
            .padding(.bottom, 9)
            .padding(.top, 10)
            
        } else {
            HStack {
                Button(action: {
                    switch settingLabel {
                    case "Edit Profile":
                        positionX -= 500
                        mainView = 1
                        settingTitle = "Edit Profile"
                        print("Edit Profile")
                    case "Change Password":
                        positionX -= 500
                        mainView = 2
                        settingTitle = "Change Password"
                        print("Change Password")
                    case "Language":
                        print("Language")
                    case "Share":
                        print("Share")
                    case "Contact us":
                        print("Contact us")
                    case "About us":
                        print("About us")
                    default:
                        print("Sign out")
                    }
                })
                {
                    Image(imageName)
                        .resizable()
                        .foregroundColor(.white)
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 18, height: 18, alignment: .center)
                        .padding(.trailing, 15)
                    Text(settingLabel)
                        .font(.headline)
                        .fontWeight(.medium)
                        .foregroundColor(Color.init(UIColor(red: 0.63, green: 0.70, blue: 0.77, alpha: 1.00)))
                        .multilineTextAlignment(.center)
                        .lineLimit(1)
                    Spacer()
                    if isLabel {
                    Text("English")
                        .font(.headline)
                        .fontWeight(.semibold)
                        .foregroundColor(.white)
                        .multilineTextAlignment(.center)
                }
            }
            .padding(.leading, 20)
            .padding(.trailing, 20)
            .padding(.bottom, 15)
            .padding(.top, 15)
            }

        }
        
    }
    
}
