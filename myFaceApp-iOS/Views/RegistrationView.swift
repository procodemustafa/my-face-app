//
//  RegistrationView.swift
//  myFaceApp-iOS
//
//  Created by Mustafa Alkhazraji on 23.11.2020.
//

import SwiftUI

struct RegistrationView: View {
    
    @Binding var showRegister: Bool
    @State var showLogin = true
    @State private var name: String = ""
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var confirmPassword: String = ""

    
    var body: some View {
        VStack {
            Text("SIGN UP")
                .font(.title)
                .fontWeight(.medium)
                .foregroundColor(Color.init(UIColor(red: 0.63, green: 0.70, blue: 0.77, alpha: 1.00)))
                .multilineTextAlignment(.center)
                .padding(.bottom, 25)
            
            customTextField(isItPassword: false, text: $name, placeHolder: "Name")
            customTextField(isItPassword: false, text: $email, placeHolder: "Email")
            customTextField(isItPassword: true, text: $password, placeHolder: "Password")
            customTextField(isItPassword: true, text: $confirmPassword, placeHolder: "Confirm Password")

            Button(action: {
                print("Tapped")
            }) {
                Text("Contiune")
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .frame(height: 50)
                    .background(Color.init(UIColor(red: 0.53, green: 0.94, blue: 0.60, alpha: 1.00)))
                    .foregroundColor(.white)
                    .cornerRadius(10)
                    .padding(.leading, 25)
                    .padding(.trailing, 25)
            }
            .padding(.bottom, 10)
            
            Button(action: {
                showRegister = false
            }) {
                Text("SIGN IN")
                    .font(.subheadline)
                    .foregroundColor(Color.init(UIColor(red: 0.53, green: 0.94, blue: 0.60, alpha: 1.00)))
                    .cornerRadius(10)
            }
            if !showRegister {
                LoginView(showLogin: $showLogin)
            }
            Spacer()

        }
        .padding(.top, 30)
        .padding(.leading, 10)
        .padding(.trailing, 10)
        .padding(.bottom, 30)

    }
}

#if DEBUG
struct RegistrationView_Previews: PreviewProvider {
    static var previews: some View {
        Color.init(UIColor(red: 0.29, green: 0.35, blue: 0.40, alpha: 1.00))
             .edgesIgnoringSafeArea(.all)
        RegistrationView(showRegister: .constant(true))
    }
}
#endif
