//
//  PhoneVerificationView.swift
//  myFaceApp-iOS
//
//  Created by Mustafa Alkhazraji on 30.11.2020.
//

import SwiftUI



struct PhoneVerificationView: View {
    @ObservedObject var passcodeModel = PasscodeModel(passCodeLength: 6)
   
    @State private var attempts: Int = 0


    
       var body: some View {
        
        GeometryReader {container in
            VStack {
                Spacer()
                    .frame(height: 50)
                VStack {
                    Text("SIGN UP")
                        .fontWeight(.bold)
                        .foregroundColor(Color.init(UIColor(red: 0.63, green: 0.70, blue: 0.77, alpha: 1.00)))
                        .multilineTextAlignment(.leading)
                        .frame(minWidth: 0, maxWidth: container.size.width)
                        .padding(.bottom, 30)
                        .shadow(radius: 10, x: 3, y: 6)
                    Text("Enter Verification Code")
                        .fontWeight(.bold)
                        .foregroundColor(Color.init(UIColor(red: 0.63, green: 0.70, blue: 0.77, alpha: 1.00)))
                        .multilineTextAlignment(.leading)
                        .frame(minWidth: 0, maxWidth: container.size.width)
                        .padding(.bottom, 10)
                        .shadow(radius: 10, x: 3, y: 6)
                    
                    HStack {
                        PassCodeInputField(inputModel: self.passcodeModel)
                            .modifier(Shake(animatableData: CGFloat(attempts)))
                    }
                    .padding(.bottom, 25)
                    .frame(minWidth: 0, maxWidth: container.size.width)
                    Button(action: {
                        
         //               if let Correct {
                            print("Passcode is \(self.passcodeModel.passCodeString)")
         //               } else {
                            withAnimation(.default) {
                                self.attempts += 1
                                
                                
                            }
        //                }
                        
                    }) {
                        Text("Confirm")
                            .frame(minWidth: 0, maxWidth: container.size.width)
                            .frame(height: 50)
                            .background(!self.passcodeModel.isValid ? Color.init(UIColor(red: 0.35, green: 0.41, blue: 0.46, alpha: 1.00)) : Color.init(UIColor(red: 0.53, green: 0.94, blue: 0.60, alpha: 1.00)))
                            .foregroundColor(.white)
                            .cornerRadius(10)
                            .frame(minWidth: 0, maxWidth: container.size.width)

                    }
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .padding(.leading, 25)
                    .padding(.trailing, 25)
                    .padding(.bottom, 10)
                    .shadow(radius: 10, x: 3, y: 6)
                    .disabled(!self.passcodeModel.isValid)
                    Button(action: {
                        print("Tapped")
                    }) {
                        Text("Resend Verification Code")
                            .font(.subheadline)
                            .fontWeight(.regular)
                            .frame(height: 50)
                            .foregroundColor(Color.init(UIColor(red: 0.53, green: 0.94, blue: 0.60, alpha: 1.00)))
                            .cornerRadius(10)
                            .padding(.leading, 25)
                            .padding(.trailing, 25)
                    }
                }
            }
            
        }
       }
}
#if DEBUG
struct PhoneVerificationView_Previews: PreviewProvider {
    static var previews: some View {
        PhoneVerificationView()
    }
}
#endif
