//
//  WelcomeView.swift
//  myFaceApp-iOS
//
//  Created by Mustafa Alkhazraji on 23.11.2020.
//

import SwiftUI


struct WelcomeView: View {
    
    // @ObservedObject var store = DataStore()
    var api = ApiTest()
    
    @State var isNavigationBarHidden: Bool = true
    @State var tutorialStage = 0
    @State var showLogin = false
    @State var viewState = CGSize.zero
    @State var imageViewState = CGSize.zero
    @State var blur = true
    @State var tutorialText: String = "Description 1"
    @State var dragON = false


    var body: some View {
        
        ZStack {
            Color.init(UIColor(red: 0.29, green: 0.35, blue: 0.40, alpha: 1.00))
                 .edgesIgnoringSafeArea(.all)
            VStack {
                    Image("Logo")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 320, height: 320)
                        .padding(.leading, 25)
                        .padding(.trailing, 25)
                        .padding(.bottom, 10)
                    
                    Spacer()
                    
                    Text("Welcome")
                        .font(.title)
                        .multilineTextAlignment(.center)
                        .foregroundColor(Color.init(UIColor(red: 0.69, green: 0.77, blue: 0.84, alpha: 1.00)))
                        .padding(.leading, 25)
                        .padding(.trailing, 25)
                        .padding(.bottom, 10)
                    
                    Text("Description")
                        .font(.body)
                        .foregroundColor(Color.init(UIColor(red: 0.61, green: 0.68, blue: 0.75, alpha: 1.00)))
                        .multilineTextAlignment(.center)
                        .lineLimit(4)
                        .padding(.leading, 25)
                        .padding(.trailing, 25)
                        .padding(.bottom, 10)
                
                    Button(action: {
                        self.tutorialStage = 1
                        
                        api.fetchData { (dict, error) in
                                debugPrint(dict)
                            }
                        
                        
                    }) {
                            Text("NEXT")
                                .frame(width: 92, height: 42, alignment: .center)
                                .frame(maxWidth: 92)
                                .background(Color.init(UIColor(red: 0.53, green: 0.94, blue: 0.60, alpha: 1.00)))
                                .foregroundColor(.white)
                                .cornerRadius(10)
                                .padding(.bottom, 20)
                                .padding(.leading, (self.tutorialStage == 4) ? 25 : 20)
                                .padding(.trailing, (self.tutorialStage == 4) ? 25 : 20)
                        }
                    Spacer()
                }
                .padding(.top, 30)
                .padding(.leading, 10)
                .padding(.trailing, 10)
                .padding(.bottom, 30)
            
            if (self.tutorialStage == 1 || self.tutorialStage != 0) {
                GeometryReader { container in
                    ZStack {
                        Color.init(UIColor(red: 0.29, green: 0.35, blue: 0.40, alpha: 1.00))
                             .edgesIgnoringSafeArea(.all)
                        VStack {
                            Image("TutorialAvatar")
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .frame(width: container.size.width)
                                .offset(x: -20)
                                .offset(y: (self.tutorialStage == 4) ? -250 : 0)
                                //.grayscale( (self.tutorialStage == 4) ? 0.99 : 0)
                                .grayscale( (!self.dragON && self.tutorialStage == 4) ? 0.99 : 0)
                                .blur(radius: blur ? 20 : 0)

                        }
                        .edgesIgnoringSafeArea(.all)
                        .offset(y: imageViewState.height)
                        .gesture(
                            DragGesture()
                                .onChanged { value in
                                    if self.tutorialStage == 4 {
                                        self.imageViewState = value.translation
                                        self.dragON = true
                                    }
                                }
                                .onEnded { value in
                                    if self.tutorialStage == 4 {
                                        self.imageViewState = .zero
                                        self.dragON = false
                                    }
                                }
                        )
 
                        if self.tutorialStage == 2 || self.tutorialStage == 3 {
                            Image("TutorialFocus")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 250, height: 250, alignment: .center)
                                .offset(x: 0, y: (self.tutorialStage == 4) ? -250 : -20)

                        }
                        if self.tutorialStage == 3 {
                            Image("TutorialCheck")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 45, height: 45, alignment: .center)
                                .offset(x: 0, y: 90)

                        }
                        
                        VStack {
                            if self.tutorialStage != 1 {
                                Spacer()
                            }
                            VStack {
                                VStack() {
                                    //Description
                                    HStack { if self.tutorialStage != 4 {
                                        TutorialLabel(text: self.tutorialText)
                                    } else if self.tutorialStage == 4 {
                                            VStack {
                                                ScrollView {
                                                    AvatarView(image: "Avatar1", text: "Maria Winter", percentage: "99%")
                                                    AvatarView(image: "Avatar2", text: "Grant Marshal", percentage: "14%")
                                                    AvatarView(image: "Avatar3", text: "Duran Clayton", percentage: "8%")
                                                }
                                            }
                                        }
                                    if self.tutorialStage != 4 {
                                            Spacer()
                                        }
                                    }

                                    Spacer()
                                    //Button
                                    HStack {
                                        if self.tutorialStage != 4 {
                                            Spacer()
                                        }
                                            Button(action: {
                                                if self.tutorialStage == 1 {
                                                    self.tutorialText = "Description 2"
                                                    self.tutorialStage = 2
                                                    blur = false
                                                } else if self.tutorialStage == 2 {
                                                    self.tutorialText = "Description 3"
                                                    self.tutorialStage = 3
                                                } else if self.tutorialStage == 3 {
                                                    self.tutorialText = "Description 4"
                                                    self.tutorialStage = 4
                                                } else if self.tutorialStage == 4 {
                                                    showLogin = true
                                                }

                                            }) {
                                                Text((self.tutorialStage == 4) ? "Try yourself!" : "NEXT")
                                                    .frame(width: (self.tutorialStage == 4) ? .none : 92, height: 42, alignment: .center)
                                                    .frame(maxWidth: (self.tutorialStage == 4) ? .infinity : 92)
                                                    .background(Color.init(UIColor(red: 0.53, green: 0.94, blue: 0.60, alpha: 1.00)))
                                                    .foregroundColor(.white)
                                                    .cornerRadius(10)
                                                    .padding(.bottom, 20)
                                                    .padding(.leading, (self.tutorialStage == 4) ? 25 : 20)
                                                    .padding(.trailing, (self.tutorialStage == 4) ? 25 : 20)

                                            }
                                    }
                                }
                                .background(Color.init(UIColor(red: 0.29, green: 0.35, blue: 0.40, alpha: 1.00)))
                                .frame(height: (self.tutorialStage == 4) ? 460 : 180)
                                .clipShape(RoundedRectangle(cornerRadius: 20, style: .continuous))
                                .padding(.bottom, 20)
                                .padding(.leading, (self.tutorialStage == 4) ? 0 : 20)
                                .padding(.trailing, (self.tutorialStage == 4) ? 0 : 20)
                                .offset(x: viewState.width, y: viewState.height)
                                .animation(.timingCurve(0.2, 0.8, 0.2, 1, duration: 0.8))
                                .gesture(
                                    DragGesture()
                                        .onChanged { value in
                                            if self.tutorialStage == 1 {
                                                self.viewState = value.translation
                                                self.blur = false
                                            }
                                        }
                                        .onEnded { value in
                                            if self.tutorialStage == 1 {
                                                self.viewState = .zero
                                                self.blur = true
                                            }
                                        }
                                )
                                
                            }
  
                        }
                    }
                }
            }
            if self.showLogin {
                Color.init(UIColor(red: 0.29, green: 0.35, blue: 0.40, alpha: 1.00))
                     .edgesIgnoringSafeArea(.all)
                LoginView(showLogin: $showLogin)
            }
        }
        .animation(.spring(response: 0.7, dampingFraction: 0.8, blendDuration: 0))
    }
}

#if DEBUG
struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
    }
}
#endif

struct TutorialLabel: View {
    var text: String
    var body: some View {
        Text(text)
            .font(.body)
            .foregroundColor(Color.init(UIColor(red: 0.61, green: 0.68, blue: 0.75, alpha: 1.00)))
            .multilineTextAlignment(.leading)
            .lineLimit(3)
            .padding(.leading, 25)
            .padding(.trailing, 25)
            .padding(.bottom, 10)
            .padding(.top, 20)

    }
}
