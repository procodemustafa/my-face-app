//
//  PasscodeView.swift
//  myFaceApp-iOS
//
//  Created by Mustafa Alkhazraji on 30.11.2020.
//

import SwiftUI
import Foundation

struct PassCodeInputField: View {
    
    @ObservedObject var inputModel: PasscodeModel
    
    var body: some View {
        HStack {
            ForEach(0 ..< self.inputModel.numberOfCells) { index in
                PassCodeCell(index: index, selectedCellIndex: self.$inputModel.selectedCellIndex, textReference: self.$inputModel.passCode[index])
                    .frame(width: 47, height: 50, alignment: .center)
                    .background(Color.init(UIColor(red: 0.35, green: 0.41, blue: 0.46, alpha: 1.00)))
                    .font(.title)
                    .multilineTextAlignment(.center)
                    .cornerRadius(10)
                    .shadow(radius: 10, x: 3, y: 6)
                    .keyboardType(.numberPad)
            }
        }
    }
}

struct customTextField: View {
    var isItPassword: Bool
    @Binding var text: String
    var placeHolder: String
    
    var body: some View {

        ZStack(alignment: .leading) {
            if text.isEmpty {
                Text(self.placeHolder)
                    .foregroundColor(Color.init(UIColor(red: 0.52, green: 0.60, blue: 0.66, alpha: 1.00)))
                    .padding(.leading, 20)
            }
            if isItPassword {
                SecureField("", text: $text)
                    .padding(.leading, 20)
                    .padding(.trailing, 20)

            } else {
                TextField("", text: $text)
                    .padding(.leading, 20)
                    .padding(.trailing, 20)
            }
        }
        .frame(height: 50)
        .background(Color.init(UIColor(red: 0.35, green: 0.41, blue: 0.46, alpha: 1.00)))
        .foregroundColor(.white)
        .cornerRadius(10)
        .padding(.leading, 25)
        .padding(.trailing, 25)
        .padding(.bottom, 10)

    }
}

struct errorText: View {
    var title: String
    
    var body: some View {
        Text(title)
            .fontWeight(.bold)
            .foregroundColor(Color.init(UIColor(red: 0.84, green: 0.00, blue: 0.00, alpha: 1.00)))
            .multilineTextAlignment(.leading)
            .frame(minWidth: 0, maxWidth: .infinity)
            .padding(.bottom, 10)
            .shadow(radius: 10, x: 3, y: 6)
    }
}

struct AvatarView: View {
    var image: String
    var text: String
    var percentage: String
    var body: some View {
        VStack {
            HStack {
                Image(image)
                VStack(alignment: .leading) {
                    Text(text)
                        .font(.headline)
                        .foregroundColor(Color.init(UIColor(red: 0.61, green: 0.68, blue: 0.75, alpha: 1.00)))
                    Text(percentage)
                        .font(.subheadline)
                        .foregroundColor(Color.init(UIColor(red: 0.51, green: 0.91, blue: 0.58, alpha: 1.00)))
                    Spacer()
                }
                .padding(.top, 20)
                .padding(.leading, 5)
                .frame(width: .infinity, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .leading)
            Spacer()
            }
            .padding(.leading, 20)
            .padding(.trailing, 20)
            Rectangle()
                .foregroundColor(.white)
                .frame(width: .infinity, height: 2)
                .padding(.leading, 20)
                .padding(.trailing, 20)
        }
        .padding(.top, 10)
        .padding(.leading, 10)
        .padding(.trailing, 10)
        
    }
}

