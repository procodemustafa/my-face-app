//
//  LoginView.swift
//  myFaceApp-iOS
//
//  Created by Mustafa Alkhazraji on 23.11.2020.
//

import SwiftUI
//
//struct LoginView: View {
//
//
//
//    var body: some View {
//        VStack {
//            LoginForm()
//        }
//    }
//}

struct Shake: GeometryEffect {
    
    var amount: CGFloat = 10
    var shakesPerUnit = 3
    var animatableData: CGFloat

    func effectValue(size: CGSize) -> ProjectionTransform {
        ProjectionTransform(CGAffineTransform(translationX:
            amount * sin(animatableData * .pi * CGFloat(shakesPerUnit)),
            y: 0))
    }
}

struct LoginView: View {
    
    var authentication = Authentication()
    
    @Binding var showLogin: Bool
    @State var showPhone = false
    @State var loginRegisterOn = true
    @State private var attempts: Int = 0
    @State var registerNext: Bool = false
    @State var showRegister: Bool = false
    @State var showPhoneNumber: Bool = false
    @State var showErrorMessage: Bool = false
    @State var incorrectEmail: Bool = false
    @State var passwordNotMatch: Bool = false
    @State var invalidEmail: Bool = false
    @State var error: Bool = false
    @State private var userName: String = ""
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var confirmPassword: String = ""
    @State var viewState = CGSize.zero
    @State var showOptionView = false

    func onLoginButtonPress() -> Void {
        authentication.loginCompletionHandler { (status, message) in
            if status {
                print(message)
            } else {
                print(message)
            }
        }
        authentication.authenticateUserWith(email, andPassword: password)
        
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    var body: some View {
        ZStack {
            VStack {
                if !showRegister {
                    Image("Logo")
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 200 , height: 200)
                        .padding(.leading, 25)
                        .padding(.trailing, 25)
                        .padding(.bottom, 25)
                }
                Text(showRegister ? "Sign up" : "Login")
                    .font(.title)
                    .fontWeight(.medium)
                    .foregroundColor(Color.init(UIColor(red: 0.63, green: 0.70, blue: 0.77, alpha: 1.00)))
                    .multilineTextAlignment(.center)
                    .padding(.bottom, 10)
                
                    customTextField(isItPassword: false, text: $userName, placeHolder: "User Name")
                        .modifier(Shake(animatableData: CGFloat(attempts)))

                    customTextField(isItPassword: showRegister ? false : true, text: showRegister ? $email : $password, placeHolder: showRegister ? "Email" : "Password")
                        .modifier(Shake(animatableData: CGFloat(attempts)))

                if showRegister {
                    customTextField(isItPassword: true, text: $password, placeHolder: "Password")
                        .modifier(Shake(animatableData: CGFloat(attempts)))
                    customTextField(isItPassword: true, text: $confirmPassword, placeHolder: "Confirm Password")
                        .modifier(Shake(animatableData: CGFloat(attempts)))
                }
                if error {
                    if incorrectEmail {
                        errorText(title: "Incorect Email Address")
                    } else if invalidEmail {
                        errorText(title: "Invalid Email Address")
                    } else if passwordNotMatch {
                        errorText(title: "Password Not Match")
                    }
                }
                
                Button(action: {
                    if showRegister {
                        if self.email == "" || self.password == "" || self.userName == "" || self.confirmPassword == "" {
                            withAnimation(.default) {
                                self.attempts += 1
                            }
                        } else if !isValidEmail(email) {
                            self.error = true
                            self.incorrectEmail = true
                        } else if self.password != self.confirmPassword {
                            self.error = true
                            self.passwordNotMatch = true
                        }
                        // Here should put code for register
                    } else {
                        if self.email == "" && self.password == "" {
                            self.error = false
                            withAnimation(.default) {
                                self.attempts += 1
                            }
                    } else {
                        self.error = false
                        self.showPhone = true
                        // self.onLoginButtonPress()
                        // Here should put code for Login
                        }
                    }
                }) {
                    Text(showRegister ? "Register" : "Login")
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .frame(height: 50)
                        .background(Color.init(UIColor(red: 0.53, green: 0.94, blue: 0.60, alpha: 1.00)))
                        .foregroundColor(.white)
                        .cornerRadius(10)
                        .padding(.leading, 25)
                        .padding(.trailing, 25)
                }
                .padding(.bottom, 10)
                .tag("loginButton")
                
                Button(action: {
                    self.showRegister.toggle()
                    self.error = false
                    self.userName = ""
                    self.email = ""
                    self.password = ""
                    self.confirmPassword = ""
                }) {
                    Text(showRegister ? "SIGN IN" : "SIGN UP")
                        .font(.subheadline)
                        .foregroundColor(Color.init(UIColor(red: 0.53, green: 0.94, blue: 0.60, alpha: 1.00)))
                        .cornerRadius(10)
                }
                Image(systemName: "gearshape.fill")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .foregroundColor(.white)
                    .frame(width: 32, height: 32)
                    .padding()
                    .onTapGesture {
                        showOptionView = true
                    }
                Spacer()
            }
            .padding(.top, 30)
            .padding(.leading, 10)
            .padding(.trailing, 10)
            .padding(.bottom, 30)
            .animation(.spring(response: 0.8, dampingFraction: 0.9, blendDuration: 0))
            .offset(x: viewState.width, y: viewState.height)
        }
        if showPhone {
            Color.init(UIColor(red: 0.29, green: 0.35, blue: 0.40, alpha: 1.00))
                 .edgesIgnoringSafeArea(.all)
            PhoneView(showPhone: $showPhone)
                .animation(.spring(response: 0.8, dampingFraction: 0.9, blendDuration: 0))

        }
        if showOptionView {
            Color.init(UIColor(red: 0.29, green: 0.35, blue: 0.40, alpha: 1.00))
                 .edgesIgnoringSafeArea(.all)
            OptionView()
                .animation(.spring(response: 0.8, dampingFraction: 0.9, blendDuration: 0))
        }
    }
}
        




#if DEBUG
struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.init(UIColor(red: 0.29, green: 0.35, blue: 0.40, alpha: 1.00))
                 .edgesIgnoringSafeArea(.all)
            LoginView(showLogin: .constant(true))
        }
    }
}
#endif
