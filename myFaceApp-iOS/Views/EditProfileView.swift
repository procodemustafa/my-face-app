//
//  EditProfileView.swift
//  myFaceApp-iOS
//
//  Created by Mustafa Alkhazraji on 6.12.2020.
//

import SwiftUI

struct EditProfileView: View {
    
    @State var fullName: String = ""
    @State var email: String = ""
    @State var age: String = ""
    @State var phoneNumber: String = ""
    @State private var attempts: Int = 0

    
    
    var body: some View {
        VStack {
            ScrollView {
                customTextField(isItPassword: false, text: $fullName, placeHolder: "Full Name")
                    .modifier(Shake(animatableData: CGFloat(attempts)))
                customTextField(isItPassword: true, text: $email, placeHolder: "Email")
                    .modifier(Shake(animatableData: CGFloat(attempts)))
                Button(action: {
                    if self.fullName == "" || self.email == "" || self.age == "" || self.phoneNumber == "" {
                        withAnimation(.default) {
                            self.attempts += 1
                        }
                    }
                }) {
                    Text("Continue")
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .frame(height: 50)
                        .background(Color.init(UIColor(red: 0.53, green: 0.94, blue: 0.60, alpha: 1.00)))
                        .foregroundColor(.white)
                        .cornerRadius(10)
                        .padding(.leading, 25)
                        .padding(.trailing, 25)
                }
                .padding(.bottom, 10)
            }
        }
        .animation(.spring(response: 0.9, dampingFraction: 0.7, blendDuration: 0))
    }
}


struct EditProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.init(UIColor(red: 0.29, green: 0.35, blue: 0.40, alpha: 1.00))
                 .edgesIgnoringSafeArea(.all)
            EditProfileView(fullName: "", email: "", age: "", phoneNumber: "")
        }
    }
}
