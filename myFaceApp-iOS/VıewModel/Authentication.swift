//
//  Authentication.swift
//  myFaceApp-iOS
//
//  Created by Mustafa Alkhazraji on 24.11.2020.
//

import Foundation


class Authentication: NSObject {
    
    var user: Users!
    var email: String { return user.email }
    var password: String { return user.password }
    
    typealias authenticationLoginCallBack = (_ status:Bool, _ message:String) -> Void
    var loginCallback:authenticationLoginCallBack?
    
    func authenticateUserWith(_ email:String, andPassword password:String) {
            if email.count  != 0 {
                if password.count != 0 {
                    self.verifyUserWith(email, andPassword: password)
                } else {
                    //password empty
                    self.loginCallback?(false, "Password should not be empty")
                }
            } else {
                // username empty
                self.loginCallback?(false, "Email should not be empty")
            }
    }
    
    
    //MARK:- verifyUserWith 
    fileprivate func verifyUserWith(_ email:String, andPassword password:String) {
        if email == "Test" && password == "123456" {
            user = Users(email: email, password: password)
            self.loginCallback?(true, "User is successfully authenticated")
        } else {
            // invalid credentials
            self.loginCallback?(false, "Please enter valid crenetials")
        }
    }
    
    //MARK:- loginCompletionHandler
    func loginCompletionHandler(callBack: @escaping authenticationLoginCallBack) {
        self.loginCallback = callBack
    }
}
