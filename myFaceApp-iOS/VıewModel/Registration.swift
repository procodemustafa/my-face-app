//
//  Registration.swift
//  myFaceApp-iOS
//
//  Created by Mustafa Alkhazraji on 24.11.2020.
//

import Foundation


class registration: NSObject {
 
    var user: Users!
    var name: String { return user.name }
    var email: String { return user.email }
    var password: String { return user.password }
    var phoneNumber: String { return user.phoneNumber }
    
    typealias registrationCallBack = (_ status:Bool, _ message:String) -> Void
    var registrationCallback:registrationCallBack?
    
    func registerUserWith(_ email:String, andPassword password:String) {
            if email.count  != 0 {
                if password.count != 0 {
                    self.verifyUserWith(email, andPassword: password)
                } else {
                    self.registrationCallback?(false, "Password should not be empty")
                }
            } else {
                self.registrationCallback?(false, "Email should not be empty")
            }
    }
    
    
    fileprivate func verifyUserWith(_ email:String, andPassword password:String) {
        // Code here
        if email == "test" && password == "123456" {
            user = Users(email: email, password: password)
            self.registrationCallback?(true, "User is successfully authenticated")
        } else {
            self.registrationCallback?(false, "Please enter valid crenetials")
        }
    }
    
    //MARK:- loginCompletionHandler
    func registartionCompletionHandler(callBack: @escaping registrationCallBack) {
        self.registrationCallback = callBack
    }
}
