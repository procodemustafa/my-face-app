//
//  Users.swift
//  myFaceApp-iOS
//
//  Created by Mustafa Alkhazraji on 22.11.2020.
//

import SwiftUI

struct Post: Codable, Identifiable {
    let id = UUID()
    var status: String
    var code: String
    var message: String
    var results: [String]
}

class ApiTest {
//    func getPosts(completion: @escaping ([Post]) -> ()) {
//        guard let url = URL(string: "https://eu-apis.my-face.app/api/v1/app-settings/by?region=eu&lang_code=en-us") else { return }
//
//        URLSession.shared.dataTask(with: url) { (data, _, _) in
//            guard let data = data else { return }
//
//            let posts = try! JSONDecoder().decode([Post].self, from: data)
//
//            DispatchQueue.main.async {
//                completion(posts)
//            }
//        }
//        .resume()
//    }
    func fetchData(completion: @escaping ([String:Any]?, Error?) -> Void) {
        let url = URL(string: "https://eu-apis.my-face.app/api/v1/app-settings/by?region=eu&lang_code=en-us")!

        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            do {
                if let array = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any]{
                    completion(array, nil)
                }
            } catch {
                print(error)
                completion(nil, error)
            }
        }
        task.resume()
    }
}
