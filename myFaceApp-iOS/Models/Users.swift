//
//  Users.swift
//  myFaceApp-iOS
//
//  Created by Mustafa Alkhazraji on 6.12.2020.
//

import Foundation

class Users: NSObject {
    
    var name : String = ""
    var email : String = ""
    var password : String = ""
    var phoneNumber : String = ""
    
    
    init(name: String, email : String, password : String, phoneNumber: String) {
        
        self.name = name
        self.email  = email
        self.password = password
        self.phoneNumber = phoneNumber
        
    }
    
    init(email : String, password : String) {
        
        self.email  = email
        self.password = password
        
    }


}


